package ajaymehta.staticobject;

/**
 * Created by Avi Hacker on 7/18/2017.
 */

public class Movie {

    private String movieName;

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}
