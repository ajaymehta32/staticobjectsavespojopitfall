package ajaymehta.staticobject;

/**
 * Created by Avi Hacker on 7/18/2017.
 */

// when u make a object static ... you can use the same object in difference classes ..coz ..it coplies the same object ..
    // it doesnt not create a new one... so so so ...u can move single object around the whole project
    // saves us from getter setter pitfall ....

// waise we always make object static so that we can use that in static method ...this is the another reason for creating the object static ..

public class SetData {

  //  static   Movie movie =  new Movie();     // creating object outside also works...

    static   Movie movie;  // but we creating it globally...

    public static void main(String args[]) {

        setData();
    }


    public static void setData() {

        movie =  new Movie();

 // setting movie name  n also ..setting the object ..
        // so that we could know ..jaha ye movie name get ho raha hai ..yaha kya ye same object bhi ja rah hai
        // we know ki same object ja rha hai coz humare getter main null nai aa rha ..value aa rhi hai...just to let you know i printed the object too koi dikat..
        movie.setMovieName("Sniper " +"object -> "+movie.toString());

        new PrintData().printData();
        new PrintData2().printData2();



    }
}
